package com.example.simple;


import org.xutils.common.Callback.CancelledException;
import org.xutils.common.Callback.CommonCallback;

import com.androidframex.LG.LG;
import com.androidframex.init.BaseActivityX;
import com.androidframex.udp.SendUdpX;
import com.androidframex.view.ToastX;
import com.androidframex.view.ViewX;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends BaseActivityX{

	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initbackToolbar();
		setContentView(R.layout.activity_main);
		setTitle("有英雄的国家是可悲的");

	}
	
	private TextView textView1;
	private Button button1;
	private Button button2;
	private Button button3;
	private Button button4;


	private void bindview(){
		textView1=(TextView) findViewById(R.id.textView1);
		button1=(Button) findViewById(R.id.button1);
		button2=(Button) findViewById(R.id.button2);
		button3=(Button) findViewById(R.id.button3);
		button4=(Button) findViewById(R.id.button4);
	}
	
	private void onclick(){
		//打印日志
		button1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LG.d("test");
			}
		});
		
		button2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ToastX.ToastSuccess(MainActivity.this,"Toast");
			}
		});
		button3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SendUdpX send=new SendUdpX("192.168.12.1",123);
				send.sendUdpData("test", new CommonCallback<String>() {
					
					@Override
					public void onSuccess(String arg0) {
						// TODO Auto-generated method stub
						ToastX.ToastSuccess(MainActivity.this,"udp success");
					}
					
					@Override
					public void onFinished() {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onError(Throwable arg0, boolean arg1) {
						// TODO Auto-generated method stub
						ToastX.ToastFair(MainActivity.this,"udp error");
					}
					
					@Override
					public void onCancelled(CancelledException arg0) {
						// TODO Auto-generated method stub
						
					}
				});
			}
		});
		button4.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ViewX.showProgressDialog(MainActivity.this);
				try {
					ViewX.setProgressDialogMsg("等待...");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mhandler.sendEmptyMessageDelayed(0,3000);
			}
		});
	}

		private Handler mhandler=new Handler(){
			
			public void handleMessage(android.os.Message msg) {
				
				switch (msg.what) {
				case 0:
					try {
						ViewX.dissmissProgressDialog();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;

				default:
					break;
				}
			};
			
		};

}
