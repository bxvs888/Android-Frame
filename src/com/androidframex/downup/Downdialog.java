package com.androidframex.downup;

import com.example.androidframex.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Downdialog extends Dialog{
	
	private TextView tv_name;
	private ProgressBar pb;
	private TextView tv_num;

	public Downdialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		// TODO Auto-generated constructor stub
	}

	public Downdialog(Context context, int themeResId) {
		super(context, 0);
		// TODO Auto-generated constructor stub
	}

	public Downdialog(Context context) {
		// TODO Auto-generated constructor stub
		   super(context,R.style.alert_dialog);
	}
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        setContentView(R.layout.dwondialog);
        tv_name=(TextView) findViewById(R.id.tv_name);
        pb=(ProgressBar) findViewById(R.id.probar);
        tv_num=(TextView) findViewById(R.id.tv_num);
	}
	
	public void setname(String name){
		if(tv_name!=null&&name!=null){
			tv_name.setText(name);
		}
	}
	
	public void setpb(int current){
		
		if(pb!=null){
			pb.setProgress(current);
		}
		if(tv_num!=null){
			tv_num.setText(current+"%");
		}
	}

	
	

}
