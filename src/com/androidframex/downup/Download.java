package com.androidframex.downup;
import java.io.File;

import org.xutils.x;
import org.xutils.common.Callback.ProgressCallback;

import com.androidframex.LG.LG;


import android.annotation.SuppressLint;
import android.content.Context;
import android.drm.ProcessedData;
import android.media.session.MediaSession.Callback;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.telephony.TelephonyManager;


public class Download {
	
	
	/*
	 * 检测是否使用流量
	 */
	@SuppressLint("NewApi")
	public static void CheckWifiNet(Context context){
		ConnectivityManager mConnectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);   
		NetworkInfo info = mConnectivity.getActiveNetworkInfo();   
		TelephonyManager mTelephony = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);   
		int netType = info.getType(); 
		int netSubtype = info.getSubtype(); 
		if (netType == ConnectivityManager.TYPE_WIFI) {  //WIFI
		  LG.d("wifi");
		} else if (netType == ConnectivityManager.TYPE_MOBILE && netSubtype == TelephonyManager.NETWORK_TYPE_UMTS && !mTelephony.isNetworkRoaming()) {   //MOBILE
		  LG.d("连接--2");
		} else { 
		 LG.d("--3");
		} 
	}
	
	public static void download(String url,ProgressCallback<File> call){
		DownloadRequestParam drp=new DownloadRequestParam(url);
		drp.setAutoRename(true);
		drp.setAutoResume(true);
		drp.setSaveFilePath(Environment.getExternalStorageDirectory()+"/"+123);
		x.http().get(drp, call);
	}

}
