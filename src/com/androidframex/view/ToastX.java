package com.androidframex.view;



import com.example.androidframex.R;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ToastX {
	
	public  static void ToastSuccess(Context cont, String msg){
		View toastRoot = ((Activity) cont).getLayoutInflater().inflate(R.layout.toast, null);
		TextView message = (TextView) toastRoot.findViewById(R.id.message);  
		ImageView iv=(ImageView) toastRoot.findViewById(R.id.notice_image);
		iv.setImageResource(R.drawable.notice_succeed);
		message.setText(msg);
		Toast toast=new Toast(cont);
		toast.setGravity(Gravity.CENTER,0,10);
		toast.setView(toastRoot);
		toast.show();
	}
	
	public static void ToastFair(Context cont, String msg){
		View toastRoot = ((Activity) cont).getLayoutInflater().inflate(R.layout.toast, null);
		TextView message = (TextView) toastRoot.findViewById(R.id.message);  
		ImageView iv=(ImageView) toastRoot.findViewById(R.id.notice_image);
		iv.setImageResource(R.drawable.notice_failure);

		message.setText(msg);
		Toast toast=new Toast(cont);
		toast.setGravity(Gravity.CENTER,0,10);
		toast.setView(toastRoot);
		toast.show();
	}

}
