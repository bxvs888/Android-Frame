package com.androidframex.view;

import android.content.Context;

/**
 * @author Administrator
 *	
 */
public class ViewX {

	private static MyProgressDialog md;
	
	public static void showProgressDialog(Context context){
		if(md==null){
			md=new MyProgressDialog(context);
			md.show();
		}
	}
	
	public static void setProgressDialogMsg(String msg) throws Exception{
		if(md!=null){
			md.setmsg(msg);
		}
	}
	
	public static void setProgressDialogMsg(int msg) throws Exception{
		if(md!=null){
			md.setmsg(msg);
		}
	}
	
	public static void dissmissProgressDialog()throws Exception{
		if(md!=null){
			md.dismiss();
			md=null;
		}
	}
	
}
