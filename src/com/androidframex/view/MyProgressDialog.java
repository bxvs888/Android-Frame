package com.androidframex.view;

import com.example.androidframex.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

public class MyProgressDialog extends Dialog{
	
	private TextView tv_msg;

	public MyProgressDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		// TODO Auto-generated constructor stub
	}

	public MyProgressDialog(Context context, int themeResId) {
		super(context, R.style.alert_dialog);
		// TODO Auto-generated constructor stub
	}

	public MyProgressDialog(Context context) {
		super(context,R.style.alert_dialog);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.progressdialg);
		setCancelable(false);
		tv_msg=(TextView) findViewById(R.id.p_tv);
		
	}
	
	public void setmsg(String msg){
		if(tv_msg==null||msg==null)
			return;
		tv_msg.setText(msg);
	}
	
	public void setmsg(int msg){
		if(tv_msg==null)
			return;
		tv_msg.setText(msg);
	}
	
	
	

}
