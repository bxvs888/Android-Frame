/**
* @Title: RecvThread.java
* @Package com.blink.router.passway
* @Description: TODO(用一句话描述该文件做什么)
* @author Administrator
* @date 2015-11-30
* @version V1.0
*/
package com.androidframex.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import org.xutils.common.Callback;
import org.xutils.common.Callback.CommonCallback;

import android.os.Handler;
import android.os.Message;

import com.androidframex.LG.LG;


/**
 * @ClassName: RecvThread
 * @Description: TODO(UDP发送线程)
 * @author Administrator
 * @date 2015-11-30
 *
 */
class RecvUdpThread extends Thread{
	
	private boolean recvboolean=false;
	private DatagramSocket ds=null;
	private String mag_back="";
	private Handler myhandler;
	private boolean issend=true;
	private CommonCallback<String> call;
	private Handler handler=new Handler(){
		
		public void handleMessage(Message msg) {
			
			switch (msg.what) {
			case 0:
				call.onSuccess(mag_back);

				break;
			case 1:
				call.onError(null, false);

				break;
			default:
				break;
			}
			
		};
		
	};
	

	
	public RecvUdpThread(Callback.CommonCallback<String> call,DatagramSocket ds) {
		this.ds=ds;
		this.call=call;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub	
			// 接收数据
			// 接收服务器反馈数据
			byte[] backbuf = new byte[8192];
			DatagramPacket backPacket = new DatagramPacket(backbuf,
					backbuf.length);
			try {
				ds.setSoTimeout(UdpVaules.OverTime*1000);
				ds.receive(backPacket);
				mag_back = new String(backPacket.getData(), 0, backPacket.getLength());
				handler.sendEmptyMessage(0);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Message msg=new Message();
				msg.what=1;
				msg.obj=e.toString();
				handler.sendMessage(msg);
			}finally{
				ds.close();
			}


	}

}
