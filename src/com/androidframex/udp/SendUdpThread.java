package com.androidframex.udp;



import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

import android.os.Handler;

import com.androidframex.LG.LG;


 class SendUdpThread extends Thread {
	
	public static Object sendUdpLock = new Object();
	public static boolean recvudp;
	private DatagramSocket socket;
	private byte[] data;
//	private Handler mHandler; 
	private InetAddress serverIP;
	private int port;
	
	public SendUdpThread(String url,int port,String data,DatagramSocket ds) {

		this.data=data.getBytes();
		this.port=port;
		try {
			serverIP = InetAddress.getByName(url);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		socket=ds;
	}

	/*
	 * cmd : int Protocol.CHANGE_PASSWD;
	 * uid : String 48b;
	 * pass_original : String 16b
	 * pass_new : String 16b
	 * 
	 * 
	 */
	private DatagramPacket makePacket() {

		DatagramPacket dp =  new DatagramPacket(data,
				data.length, 
			serverIP,
			port);
		return dp;
	}
	
	@Override
	public void run() {	
				try {	
					socket.send(makePacket());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			

	}
	

}
