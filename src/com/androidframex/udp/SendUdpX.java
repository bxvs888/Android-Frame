package com.androidframex.udp;

import java.net.DatagramSocket;
import java.net.SocketException;

import org.xutils.common.Callback;
import org.xutils.common.Callback.CommonCallback;

import android.os.Message;
import android.util.Log;



public class SendUdpX{
	

	private String ip;
	private int port;
	private DatagramSocket ds;
	
	public SendUdpX(String ip,int port) {
		// TODO Auto-generated constructor stub
		this.ip=ip;
		this.port=port;
	}
	
	public void setOverTime(int t){
		UdpVaules.OverTime=t;
	}
	
	
	public void sendUdpData(String data,CommonCallback<String> call){
		try {
			ds=new DatagramSocket();
			new SendUdpThread(ip, port,data, ds).start();
			new RecvUdpThread(call,ds).start();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			call.onError(e,false);
		}

	}
	


}
