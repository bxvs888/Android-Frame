package com.androidframex.init;



import android.app.Activity;
import android.content.Context;
import android.widget.FrameLayout;

public interface XToolbar {

	public void init(Context context,FrameLayout mContentView);
	public void hide();
	public void show();
	public void setTitle(String title);
	public void setStyle(Context context,int style);
}
