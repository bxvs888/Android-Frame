package com.androidframex.init;

import android.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;

@SuppressLint("NewApi")
public class BaseActivityX extends AppCompatActivity{
	

    private ToolBarHelper mToolBarHelper ;
    public Toolbar toolbar ;
    private boolean isScreenAll=false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mToolBarHelper=new ToolBarHelper();
	}
	
	public void setTitleX(String title){
		mToolBarHelper.setTitle(title);
	}
	
	public void setTitleX(int title){
		String str=getResources().getString(title);
		mToolBarHelper.setTitle(str);
	}
	
	public void initbackToolbar(){
		mToolBarHelper.initback();
	}
	
	public void initNormal(){
		mToolBarHelper.initnormal();
	}
	
	public void setStyle(Context context,int style){
		mToolBarHelper.setStyle(context, style);
	}

	public void initScreenAll(){
		isScreenAll=true;
	}
	
	@Override
	public void setContentView(@LayoutRes int layoutResID) {
		// TODO Auto-generated method stub
		if(isScreenAll){
			this.requestWindowFeature(Window.FEATURE_NO_TITLE);//ȥ��������
		}
		super.setContentView(layoutResID);
		mToolBarHelper.setToolbar(this,layoutResID) ;

		if(mToolBarHelper.getContentView()!=null)
        setContentView(mToolBarHelper.getContentView());

	}
	
	public void onCreateCustomToolBar(Toolbar toolbar){
        toolbar.setContentInsetsRelative(0,0);
    }

}
