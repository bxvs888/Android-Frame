package com.androidframex.init;

import org.xutils.x;

import com.orhanobut.logger.Logger;

import android.app.Application;

public class UtilsX {
	
	public static void init(Application mApplication){
		x.Ext.init(mApplication);
		Logger.init();
	}

}
